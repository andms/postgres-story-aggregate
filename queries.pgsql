-- SELECT * FROM "story" where route='the-federalist-papers';

select s.id, s.title, s.route, s.snippet, string_agg(sp.username, ',') as authors
from story s
join story_author sa on (s.id = sa.story_id)
join story_profile sp on (sa.author_id = sp.id)
WHERE s.id > 80
group BY s.id
LIMIT 10;

-- select SUBSTRING ( body, 100, 216) from story_body;
