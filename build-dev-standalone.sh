#!/bin/bash

./delete-connectors.sh

docker-compose down -v

docker login registry.gitlab.com
docker build -t registry.gitlab.com/andms/postgres-story-aggregate .
docker push registry.gitlab.com/andms/postgres-story-aggregate

docker-compose up -d

sleep 5

docker cp stories.csv postgres-story-aggregate:/tmp/stories.csv
docker cp story-bodies.csv postgres-story-aggregate:/tmp/story-bodies.csv

docker cp profiles.csv postgres-story-aggregate:/tmp/profiles.csv
docker cp story-authors.csv postgres-story-aggregate:/tmp/story-authors.csv

docker cp story-snippets.csv postgres-story-aggregate:/tmp/story-snippets.csv

docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY story(title,route,subtitle,owner_id,publication_date,last_modified_date) FROM '/tmp/stories.csv' DELIMITER ',' CSV HEADER;"
docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY story_body(id,body) FROM '/tmp/story-bodies.csv' DELIMITER ',' CSV HEADER;"

docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY story_profile(id,name,username) FROM '/tmp/profiles.csv' DELIMITER ',' CSV HEADER;"
docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY story_author(story_id,author_id) FROM '/tmp/story-authors.csv' DELIMITER ',' CSV HEADER;"

docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "CREATE TABLE temp_table (id int, snippet varchar(255) );"
docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY temp_table(id,snippet) FROM '/tmp/story-snippets.csv' DELIMITER ',' CSV HEADER;"
docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "UPDATE story SET snippet = temp_table.snippet FROM temp_table Where story.id = temp_table.id;"
docker exec -it postgres-story-aggregate psql -U 'postgres' -d 'postgres' -c "DROP TABLE temp_table;"